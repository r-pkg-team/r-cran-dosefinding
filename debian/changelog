r-cran-dosefinding (1.2-1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Charles Plessy <plessy@debian.org>  Tue, 10 Sep 2024 13:39:59 +0900

r-cran-dosefinding (1.1-1-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 09 Nov 2023 18:04:42 +0100

r-cran-dosefinding (1.0-5-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 29 Jun 2023 21:32:15 +0200

r-cran-dosefinding (1.0-4-1) unstable; urgency=medium

  * New upstream version
  * Fix permissions

 -- Andreas Tille <tille@debian.org>  Tue, 13 Jun 2023 12:35:02 +0200

r-cran-dosefinding (1.0-3-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)
  * lintian-overrides (see lintian bug #1017966)

 -- Andreas Tille <tille@debian.org>  Mon, 09 Jan 2023 14:30:21 +0100

r-cran-dosefinding (1.0-2-1) unstable; urgency=medium

  * Disable reprotest
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 12 Oct 2021 19:53:15 +0200

r-cran-dosefinding (1.0-1-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Testsuite: autopkgtest-pkg-r (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Fix autopkgtest

 -- Andreas Tille <tille@debian.org>  Fri, 17 Sep 2021 11:32:02 +0200

r-cran-dosefinding (0.9-17-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 12
  * Standards-Version: 4.4.1
  * autopkgtest: s/ADTTMP/AUTOPKGTEST_TMP/g
  * Set upstream metadata fields: Archive.

 -- Andreas Tille <tille@debian.org>  Wed, 20 Nov 2019 09:16:53 +0100

r-cran-dosefinding (0.9-16-2) unstable; urgency=medium

  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * Point Vcs fields to salsa.debian.org
  * dh-update-R to update Build-Depends

 -- Andreas Tille <tille@debian.org>  Fri, 15 Jun 2018 10:06:10 +0200

r-cran-dosefinding (0.9-16-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.1.3
  * debhelper 11

 -- Andreas Tille <tille@debian.org>  Mon, 05 Mar 2018 11:32:29 +0100

r-cran-dosefinding (0.9-15-2) unstable; urgency=medium

  * Moved packaging from SVN to Git
  * debhelper 10
  * Secure URI in watch file
  * Standards-Version: 4.1.1

 -- Andreas Tille <tille@debian.org>  Wed, 29 Nov 2017 15:22:39 +0100

r-cran-dosefinding (0.9-15-1) unstable; urgency=medium

  * New upstream version
  * Convert to dh-r
  * Canonical homepage for CRAN
  * d/watch: version=4

 -- Andreas Tille <tille@debian.org>  Wed, 16 Nov 2016 10:58:08 +0100

r-cran-dosefinding (0.9-13-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 31 Dec 2015 00:37:22 +0100

r-cran-dosefinding (0.9-12-1) unstable; urgency=medium

  * New upstream version
  * cme fix dpkg-control
  * d/copyright: some DEP5 fixes
  * Recommends: r-cran-quadprog since it is used in tests

 -- Andreas Tille <tille@debian.org>  Tue, 30 Sep 2014 18:11:06 +0200

r-cran-dosefinding (0.9-11-1) unstable; urgency=medium

  * New upstream version
  * cme fix dpkg-control
  * add autopkgtest
  * Recommends: r-cran-rsolnp, r-cran-multcomp since these are used in
    test suite

 -- Andreas Tille <tille@debian.org>  Mon, 30 Jun 2014 15:18:28 +0200

r-cran-dosefinding (0.9-9-1) unstable; urgency=low

  * Initial release (closes: #727090)

 -- Andreas Tille <tille@debian.org>  Tue, 22 Oct 2013 09:03:24 +0200
